Canu GeneFlow App
=================

Version: 1.9-01

This GeneFlow app wraps the canu long genome assembly tool. 

Inputs
------
1. input: Sequence FASTQ file.

Parameters
----------
1. genomeSize: Estimated genome/amplicon size.
2. minReadLength: Filters out all read length less than this.
3. minOverlapLength: Minimum overlap for contig-ing the reads.
4. corMinCoverage: Controls the quality of the corrected reads.
5. readSamplingCoverage: Subsamples only the first x coverage of data. Data greater than this coverage number is ignored. Useful for large datasets.
6. correctedErrorRate: Estimated corrected error rate, 0.15 for nanopore R9.
7. output: Output directory.
