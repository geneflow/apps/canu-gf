#!/bin/bash

# Canu assembly for nanopore amplicon sequencing wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## MODIFY >>> *****************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input => Sequence FASTQ File"
    echo "  --genomeSize => Genome Size"
    echo "  --minReadLength => Minimum Read Length"
    echo "  --minOverlapLength => Minimum Overlap length"
    echo "  --corMinCoverage => Correct Base Min Coverage"
    echo "  --readSamplingCoverage => Coverage Used for Assembly"
    echo "  --correctedErrorRate => Corrected Error Rate"
    echo "  --output => Output Directory"
    echo "  --exec_method => Execution method (environment, auto)"
    echo "  --help => Display this help message"
}
## ***************************************************************** <<< MODIFY

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd"
    ERROR_CODE=$?
    if [ ${ERROR_CODE} -ne 0 ]; then
        echo "Error when executing command '${cmd}'"
        exit ${ERROR_CODE}
    fi
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## MODIFY >>> *****************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,input:,genomeSize:,minReadLength:,minOverlapLength:,corMinCoverage:,readSamplingCoverage:,correctedErrorRate:,output:,
## ***************************************************************** <<< MODIFY

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## MODIFY >>> *****************************************************************
## Set any defaults for command line options
EXEC_METHOD="auto"
## ***************************************************************** <<< MODIFY

## MODIFY >>> *****************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input)
            if [ -z "${input}" ]; then
                INPUT=$2
            else
                INPUT=${input}
            fi
            shift 2
            ;;
        --genomeSize)
            if [ -z "${genomeSize}" ]; then
                GENOMESIZE=$2
            else
                GENOMESIZE=${genomeSize}
            fi
            shift 2
            ;;
        --minReadLength)
            if [ -z "${minReadLength}" ]; then
                MINREADLENGTH=$2
            else
                MINREADLENGTH=${minReadLength}
            fi
            shift 2
            ;;
        --minOverlapLength)
            if [ -z "${minOverlapLength}" ]; then
                MINOVERLAPLENGTH=$2
            else
                MINOVERLAPLENGTH=${minOverlapLength}
            fi
            shift 2
            ;;
        --corMinCoverage)
            if [ -z "${corMinCoverage}" ]; then
                CORMINCOVERAGE=$2
            else
                CORMINCOVERAGE=${corMinCoverage}
            fi
            shift 2
            ;;
        --readSamplingCoverage)
            if [ -z "${readSamplingCoverage}" ]; then
                READSAMPLINGCOVERAGE=$2
            else
                READSAMPLINGCOVERAGE=${readSamplingCoverage}
            fi
            shift 2
            ;;
        --correctedErrorRate)
            if [ -z "${correctedErrorRate}" ]; then
                CORRECTEDERRORRATE=$2
            else
                CORRECTEDERRORRATE=${correctedErrorRate}
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT=${output}
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD=${exec_method}
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ***************************************************************** <<< MODIFY

## MODIFY >>> *****************************************************************
## Log any variables passed as inputs
echo "Input: ${INPUT}"
echo "Genomesize: ${GENOMESIZE}"
echo "Minreadlength: ${MINREADLENGTH}"
echo "Minoverlaplength: ${MINOVERLAPLENGTH}"
echo "Cormincoverage: ${CORMINCOVERAGE}"
echo "Readsamplingcoverage: ${READSAMPLINGCOVERAGE}"
echo "Correctederrorrate: ${CORRECTEDERRORRATE}"
echo "Output: ${OUTPUT}"
echo "Execution Method: ${EXEC_METHOD}"
## ***************************************************************** <<< MODIFY



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## MODIFY >>> *****************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT input

if [ -z "${INPUT}" ]; then
    echo "Sequence FASTQ File required"
    echo
    usage
    exit 1
fi
# make sure INPUT is staged
count=0
while [ ! -f "${INPUT}" ]
do
    echo "${INPUT} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -f "${INPUT}" ]; then
    echo "Sequence FASTQ File not found: ${INPUT}"
    exit 1
fi
INPUT_FULL=$(readlink -f "${INPUT}")
INPUT_DIR=$(dirname "${INPUT_FULL}")
INPUT_BASE=$(basename "${INPUT_FULL}")



# GENOMESIZE parameter
if [ -n "${GENOMESIZE}" ]; then
    :
else
    :
    echo "Genome Size required"
    echo
    usage
    exit 1
fi


# MINREADLENGTH parameter
if [ -n "${MINREADLENGTH}" ]; then
    :
else
    :
    echo "Minimum Read Length required"
    echo
    usage
    exit 1
fi


# MINOVERLAPLENGTH parameter
if [ -n "${MINOVERLAPLENGTH}" ]; then
    :
else
    :
    echo "Minimum Overlap length required"
    echo
    usage
    exit 1
fi


# CORMINCOVERAGE parameter
if [ -n "${CORMINCOVERAGE}" ]; then
    :
else
    :
    echo "Correct Base Min Coverage required"
    echo
    usage
    exit 1
fi


# READSAMPLINGCOVERAGE parameter
if [ -n "${READSAMPLINGCOVERAGE}" ]; then
    :
else
    :
    echo "Coverage Used for Assembly required"
    echo
    usage
    exit 1
fi


# CORRECTEDERRORRATE parameter
if [ -n "${CORRECTEDERRORRATE}" ]; then
    :
else
    :
    echo "Corrected Error Rate required"
    echo
    usage
    exit 1
fi


# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output Directory required"
    echo
    usage
    exit 1
fi


## ***************************************************************** <<< MODIFY

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   package: binaries packaged with the app
##   cdc-shared-package: binaries centrally located at the CDC
##   singularity: singularity image packaged with the app
##   cdc-shared-singularity: singularity image centrally located at the CDC
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path
##   module: environment modules

## MODIFY >>> *****************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="environment auto"
## ***************************************************************** <<< MODIFY

# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## MODIFY >>> *****************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect if singularity available
    if command -v singularity >/dev/null 2>&1; then
        SINGULARITY=yes
    else
        SINGULARITY=no
    fi

    # detect if docker available
    if command -v docker >/dev/null 2>&1; then
        DOCKER=yes
    else
        DOCKER=no
    fi

    # detect execution method
    if command -v canu >/dev/null 2>&1; then
        AUTO_EXEC=environment
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## MODIFY >>> *****************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${OUTPUT_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
MNT=""; ARG=""; CMD0="mkdir -p ${LOG_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ***************************************************************** <<< MODIFY



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## MODIFY >>> *****************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    environment)
        MNT=""; ARG=""; ARG="${ARG} -p"; ARG="${ARG} \"asm\""; ARG="${ARG} -d"; ARG="${ARG} \"${OUTPUT_FULL}\""; ARG="${ARG} \"useGrid=0\""; ARG="${ARG} -nanopore-raw"; ARG="${ARG} \"${INPUT_FULL}\""; ARG="${ARG} \"genomeSize=${GENOMESIZE}\""; ARG="${ARG} \"stopOnReadQuality=false\""; ARG="${ARG} \"minReadLength=${MINREADLENGTH}\""; ARG="${ARG} \"minOverlapLength=${MINOVERLAPLENGTH}\""; ARG="${ARG} \"corMinCoverage=${CORMINCOVERAGE}\""; ARG="${ARG} \"readSamplingCoverage=${READSAMPLINGCOVERAGE}\""; ARG="${ARG} \"correctedErrorRate=${CORRECTEDERRORRATE}\""; CMD0="canu ${ARG}"; CMD0="${CMD0} >\"${LOG_FULL}/${OUTPUT_BASE}-canu.stdout\""; CMD0="${CMD0} 2>\"${LOG_FULL}/${OUTPUT_BASE}-canu.stderr\""; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ***************************************************************** <<< MODIFY



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## MODIFY >>> *****************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ***************************************************************** <<< MODIFY

